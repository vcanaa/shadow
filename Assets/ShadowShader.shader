﻿Shader "Custom/ShadowShader" {
	Properties {
		_LightPos ("LightPos", Vector) = (0, 0, 0, 0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		Pass {
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma fragment frag
			#pragma vertex vert

			float2 _LightPos;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				

				if (v.vertex.z == 0) {
					float4 vv = mul (_Object2World, v.vertex);
					float2 d = vv.xy - _LightPos;
					vv.xy += d * 100;
					o.vertex = mul(UNITY_MATRIX_VP, vv);
				}
				else{
					o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				}
				

				return o;
			}

			fixed4 frag (v2f i) : SV_Target {
				return fixed4(0, 0, 0, 1);
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
