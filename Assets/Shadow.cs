﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shadow : MonoBehaviour {

    public float size;
    public Transform lightPos;

    Material mat;

    void Awake() {
        Mesh m = new Mesh();
        m.name = "ShadowMesh";
        
        m.vertices = new Vector3[] {
            new Vector3(-size / 2, -size / 2,  0f), 
            new Vector3( size / 2, -size / 2,  0f), 
            new Vector3( size / 2,  size / 2,  0f), 
            new Vector3(-size / 2,  size / 2,  0f),
            new Vector3(-size / 2, -size / 2, -1f), 
            new Vector3( size / 2, -size / 2, -1f), 
            new Vector3( size / 2,  size / 2, -1f), 
            new Vector3(-size / 2,  size / 2, -1f)
        };

        //m.uv = [Vector2 (0, 0), Vector2 (0, 1), Vector2(1, 1), Vector2 (1, 0)];
        m.triangles = new int[] { 
            1, 5, 6, 
            1, 6, 2,
            2, 6, 7,
            2, 7, 3,
            3, 7, 4,
            3, 4, 0,
            0, 4, 5,
            0, 5, 1
        };

        //m.RecalculateNormals();
       
        this.GetComponent<MeshFilter>().mesh = m;
        //var obj : GameObject = new GameObject("New_Plane_Fom_Script", MeshRenderer, MeshFilter, MeshCollider);
        //obj.GetComponent(MeshFilter).mesh = m;
    }

	// Use this for initialization
	void Start () {
        var rend = GetComponent<Renderer>();
        mat = rend.material;
    }
	
	// Update is called once per frame
	void Update () {
        var p = lightPos.position;
        mat.SetVector("_LightPos", p);
	}
}
